<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Load_model extends CI_Model
{
	
	function __construct()
	{
		$this->load->library('m_db');
	}

	function getdata($page)
	{
		$start = 1*$page;
        $limit = 1;
        $d=$this->m_db->get_data('blog','','tgl DESC','',$limit,$start);
		return $d;
	}

	function getdataKat($where=array(),$page)
	{
		$start = 1*$page;
        $limit = 1;
        $d=$this->m_db->get_data('blog',$where,'tgl DESC','',$limit,$start);
		return $d;
	}
}