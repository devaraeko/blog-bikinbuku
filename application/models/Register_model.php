<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Register_model extends CI_model
{
	function __construct()
    {
         $this->load->library('m_db');
    }

	function add_account($data)
	{
        $this->load->database();
		$this->db->insert('member',$data);
			
		return  mysql_insert_id();
	}

	function register($email,$nama,$username,$password,$tgl,$jk,$telp,$fb,$twt,$alm,$nbank,$kbank,$norek,$namarek)
	{
		$v=array(
			'email'=>$email,
			'nama'=>$nama,
			'username'=>$username,
			'password'=>$password,
			'tgl_lahir'=>$tgl,
			'jk'=>$jk,
			'telp'=>$telp,
			'fb'=>$fb,
			'twitter'=>$twt,
			'alamat'=>$alm,
			'nama_bank'=>$nbank,
			'kode_bank'=>$kbank,
			'no_rek'=>$norek,
			'nama_rek'=>$namarek,
			'aktif'=>0
		);
		if ($this->m_db->add_row('member',$v)==TRUE) {
			return true;
		}
		else {
			return false;
		}
	}

	function cancel_register($userID)
	{
		$s=array(
		'id'=>$userID,
		);
		if($this->m_db->delete_row('member',$s)==TRUE)
		{
			return true;
		}else{
			return false;
		}	
	}
		
	function changeActiveState($key)
	{
		$s=array(
		'id'=>md5($key)
		);
		if ($this->m_db->is_bof('member',$s)==FALSE) {
			$d = array(
            'aktif' => 1
        	);
        	if ($this->m_db->edit_row('member',$d,$s)==TRUE) {
				return true;
			}
			else {
				return false;
			}

		}
		else {
			return false;
		}
		
	}

	function edit_profil($memID,$email,$nama,$username,$tgl,$jk,$telp,$fb,$twt,$alm,$nbank,$kbank,$norek,$namarek)
	{
		$s=array(
			'id'=>$memID
		);
		if ($this->m_db->is_bof('member',$s)==FALSE) {
			$v=array(
				'email'=>$email,
				'nama'=>$nama,
				'username'=>$username,
				'tgl_lahir'=>$tgl,
				'jk'=>$jk,
				'telp'=>$telp,
				'fb'=>$fb,
				'twitter'=>$twt,
				'alamat'=>$alm,
				'nama_bank'=>$nbank,
				'kode_bank'=>$kbank,
				'no_rek'=>$norek,
				'nama_rek'=>$namarek
			);
			if ($this->m_db->edit_row('member',$v,$s)==TRUE) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}

	function send_reset($email)
	{
		$s=array(
		'email'=>$email,
		);
		if ($this->m_db->is_bof('member',$s)==FALSE) {
			$memID = field_value('member','email',$email,'id');
			$v=array(
				'token'=>md5($memID)
			);
			if ($this->m_db->edit_row('member',$v,$s)==TRUE) {
				$hasil['status'] = true;
				$hasil['id'] = $memID;
				$hasil['token'] = md5($memID);
			}
			else {
				$hasil['status'] = false;
			}
		}
		else
		{
			$hasil['status'] = false;
		}
		return $hasil;
	}

	function reset_sandi($IDmem,$sandi_baru)
	{
		$s=array(
		'id'=>$IDmem
		);
		if ($this->m_db->is_bof('member',$s)==FALSE) {
			$d=array(
			'password'=>$sandi_baru,
			'token'=>''
			);
			if ($this->m_db->edit_row('member',$d,$s)==TRUE) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
}