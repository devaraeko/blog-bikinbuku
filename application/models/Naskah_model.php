<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Naskah_model extends CI_Model
{
	
	function __construct()
	{
		$this->load->library('m_db');
	}

	function get_data($where=array(),$order="kode_order ASC")
	{
		$d=$this->m_db->get_data('project',$where,$order);
		return $d;
	}

	function naskah_add($memberid,$judul,$penulis,$penerbit,$kat,$kts,$htm,$wrn,$jml_ctk,$hrg_ctk,$hrg_ttl,$hrg_ksm,$pkt,$hrg_pkt,$catatan)
	{
		$tgl = date("Y-m-d H:i:s");
		$kd_order = strtotime($tgl);
		$total = $hrg_ttl+$hrg_pkt;
		$val = array(
			'kode_order'=>$kd_order,
			'member_id'=>$memberid,
			'id_paket'=>$pkt,
			'tgl_order'=>$tgl,
			'total'=>$total,
			'status'=>'menunggu'
		);

		if ($this->m_db->add_row('project',$val)==TRUE)
		{
			$projectID=$this->m_db->last_insert_id();
			$val2 = array(
				'id_project'=>$projectID,
				'judul'=>$judul,
				'penulis'=>$penulis,
				'penerbit'=>$penerbit,
				'kat_id'=>$kat,
				'id_kertas'=>$kts,
				'jml_hitam'=>$htm,
				'jml_warna'=>$wrn,
				'harga_satuan'=>$hrg_ctk,
				'harga_konsumen'=>$hrg_ksm,
				'jml_cetak'=>$jml_ctk,
				'catatan'=>$catatan
			);
			$this->m_db->add_row('project_detail',$val2);

			$hsl['status'] = true;
			$hsl['projectID'] = $projectID;
		}
		else{
			$hsl['status'] = false;
		}

		return $hsl;
	}

	function file_add($idpro,$naskah)
	{
		$d=array(
		'id_project'=>$idpro,
		'naskah'=>$naskah
		);
		if($this->m_db->add_row('file_project',$d)==TRUE)
		{
			return true;
		}else{
			return false;
		}
	}

	function getProjectbyID($id) {
		$this->db->select("*");
		$this->db->from("project");
		$this->db->join("project_detail","project.id_project=project_detail.id_project");
		$this->db->where("id_project",$id);
		$hasil = $this->db->get();
		$data = $hasil->row();
		return $data;
	}

	function naskah_data($where=array(),$order="tgl_order ASC")
	{
		$d=$this->m_db->get_data('project',$where,$order);
		return $d;
	}
	function del_psn($id)
	{
		$s=array(
			'id_project'=>$id
		);
		if ($this->m_db->delete_row('project',$s)==TRUE) {
			$IDdtl = field_value('project_detail','id_project',$id,'detail_id');
			$v=array(
				'detail_id'=>$IDdtl
			);
			if ($this->m_db->delete_row('project_detail',$v)==TRUE) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
}