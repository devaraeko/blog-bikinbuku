<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Buku_model extends CI_Model
{
	
	function __construct()
	{
		$this->load->library('m_db');
	}

	function kategori_data($where=array(),$order="nama ASC")
	{
		$d=$this->m_db->get_data('kat_buku',$where,$order);
		return $d;
	}
	
	function kategori_add($nama)
	{
		$d=array(
		'nama'=>$nama,
		);
		if($this->m_db->add_row('kat_buku',$d)==TRUE)
		{
			return true;
		}else{
			return false;
		}
	}
	
	function kategori_edit($kategoriID,$nama)
	{
		$s=array(
		'kat_id'=>$kategoriID,
		);
		$d=array(
		'nama'=>$nama
		);
		if($this->m_db->edit_row('kat_buku',$d,$s)==TRUE)
		{
			return true;
		}else{
			return false;
		}
	}
	
	function kategori_delete($kategoriID)
	{
		$s=array(
		'kat_id'=>$kategoriID,
		);
		
		if($this->m_db->delete_row('kat_buku',$s)==TRUE)
		{
			return true;
		}else{
			return false;
		}
	}

	function buku_data($where=array(),$order="judul ASC")
	{
		$d=$this->m_db->get_data('buku',$where,$order);
		return $d;
	}

	function buku_tambah($katID,$jdl,$penulis,$penerbit,$isbn,$tglterbit,$hrg,$ukuran,$hlmn,$sinop,$cover='')
	{		
		$d=array(
		'kat_id'=>$katID,
		'judul'=>$jdl,
		'member_id'=>$penulis,
		'penerbit'=>$penerbit,
		'ISBN'=>$isbn,
		'tgl_terbit'=>$tglterbit,
		'harga'=>$hrg,
		'ukuran'=>$ukuran,
		'jml_halaman'=>$hlmn,
		'sinopsis'=>$sinop,
		);
		if($this->m_db->add_row('buku',$d)==TRUE)
		{
			$bukuID=$this->m_db->last_insert_id();
			$pathupload=FCPATH.'assets/images/buku/';
			$allowtype="jpg|bmp|png|jpeg";
			$config['upload_path'] = $pathupload;
			$config['allowed_types'] = $allowtype;
			$config['max_size']	= 0;
			$config['max_filename']=0;
			$config['max_width'] = 0;
			$config['max_height'] = 0;
			$config['overwrite']=TRUE;
			if(!empty($cover))
			{
				$this->load->library('upload');
				$this->load->library('m_file');
				$count=4;
				$field="upload";
				for($i=1;$i<=$count;$i++){					
					if (!empty($_FILES[$field.$i]['name'])) {						
						$gambar=$_FILES[$field.$i]['name'];
		        		$ext=pathinfo($gambar,PATHINFO_EXTENSION);
		        		$imgname="buku_".$bukuID."-".$i.".".$ext;
		        		$config['file_name'] = $imgname;
		        		$this->upload->initialize($config);
						if ($this->upload->do_upload($field.$i))
						{							
							$sdata=$this->upload->data();
							$folder=$sdata['file_path'];
							$oripath=$sdata['full_path'];
							$imgname=$sdata['orig_name'];														
							$this->m_file->imageThumbs($pathupload,$oripath,$imgname);
							$d2=array(
							'id_buku'=>$bukuID,
							'cover'=>$imgname,
							);
							$this->m_db->add_row('buku_cover',$d2);
						}
					}
				}				
			}
			return true;
		}else{
			return false;
		}
	}

	function buku_edit($bukuID,$katID,$jdl,$penulis,$penerbit,$isbn,$tglterbit,$hrg,$ukuran,$hlmn,$sinop,$cover='')
	{
		$s=array(
		'id_buku'=>$bukuID,
		);
		$d=array(
		'kat_id'=>$katID,
		'judul'=>$jdl,
		'member_id'=>$penulis,
		'penerbit'=>$penerbit,
		'ISBN'=>$isbn,
		'tgl_terbit'=>$tglterbit,
		'harga'=>$hrg,
		'ukuran'=>$ukuran,
		'jml_halaman'=>$hlmn,
		'sinopsis'=>$sinop,
		);
		if($this->m_db->edit_row('buku',$d,$s)==TRUE)
		{			
			$pathupload=FCPATH.'assets/images/buku/';
			$allowtype="jpg|bmp|png|jpeg";
			$config['upload_path'] = $pathupload;
			$config['allowed_types'] = $allowtype;
			$config['max_size']	= 0;
			$config['max_filename']=0;
			$config['max_width'] = 0;
			$config['max_height'] = 0;
			$config['overwrite']=TRUE;
			
			if(!empty($cover))
			{
				$this->m_db->delete_row('buku_cover',array('id_buku'=>$bukuID));
				$this->load->library('upload');
				$this->load->library('m_file');
				$count=4;
				$field="upload";
				for($i=1;$i<=$count;$i++){					
					if (!empty($_FILES[$field.$i]['name'])) {						
						$gambar=$_FILES[$field.$i]['name'];
		        		$ext=pathinfo($gambar,PATHINFO_EXTENSION);
		        		$imgname="buku_".$bukuID."-".$i.".".$ext;
		        		$config['file_name'] = $imgname;
		        		$this->upload->initialize($config);
						if ($this->upload->do_upload($field.$i))
						{							
							$sdata=$this->upload->data();
							$folder=$sdata['file_path'];
							$oripath=$sdata['full_path'];
							$imgname=$sdata['orig_name'];														
							$this->m_file->imageThumbs($pathupload,$oripath,$imgname);
							$d2=array(
							'id_buku'=>$bukuID,
							'cover'=>$imgname,
							);
							$this->m_db->add_row('buku_cover',$d2);
						}
					}else{
						$last=$this->input->post('fupload'.$i);
						$d2=array(
						'id_buku'=>$bukuID,
						'cover'=>$last,
						);
						$this->m_db->add_row('buku_cover',$d2);
					}
				}				
			}
			$this->m_db->delete_row('buku_cover',array('id_buku'=>$bukuID,'cover'=>''));
			return true;
		}else{
			return false;
		}
	}

	function buku_delete($id)
	{
		$s=array(
		'id_buku'=>$id,
		);
		if($this->m_db->is_bof('buku_cover',$s)==FALSE)
		{
			$dPhoto=$this->m_db->get_data('buku_cover',$s);
			if(!empty($dPhoto))
			{
				$this->load->library('m_file');
				$pathupload=FCPATH.'assets/images/buku/';
				foreach($dPhoto as $rPhoto)
				{
					$filename=$rPhoto->cover;
					$this->m_file->deleteImage($pathupload,$filename);
				}
			}
			$this->m_db->delete_row('buku',$s);
			return true;
		}else{
			return false;
		}
	}
}