<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Konfig_model extends CI_Model
{
	
	function __construct()
	{
		$this->load->library('m_db');
	}

	function kertas_add($nama,$htm,$wrn)
	{
		$d=array(
		'nama_kertas'=>$nama,
		'hitamputih'=>$htm,
		'warna'=>$wrn,
		);
		if($this->m_db->add_row('kertas',$d)==TRUE)
		{
			return true;
		}else{
			return false;
		}
	}

	function kertas_edit($ktsID,$nama,$htm,$wrn)
	{
		$s=array(
		'id_kertas'=>$ktsID,
		);
		$d=array(
		'nama_kertas'=>$nama,
		'hitamputih'=>$htm,
		'warna'=>$wrn
		);
		if($this->m_db->edit_row('kertas',$d,$s)==TRUE)
		{
			return true;
		}else{
			return false;
		}
	}

	function kertas_delete($kertasID)
	{
		$s=array(
		'id_kertas'=>$kertasID,
		);
		
		if($this->m_db->delete_row('kertas',$s)==TRUE)
		{
			return true;
		}else{
			return false;
		}
	}

	function paket_add($nama,$harga,$det)
	{
		$d=array(
		'nama_paket'=>$nama,
		'harga'=>$harga,
		'fitur'=>$det
		);

		if($this->m_db->add_row('paket',$d)==TRUE)
		{
			return true;
		}else{
			return false;
		}
	}

	function paket_edit($pktID,$nama,$harga,$det)
	{
		$s=array(
		'id_paket'=>$pktID,
		);
		$d=array(
		'nama_paket'=>$nama,
		'harga'=>$harga,
		'fitur'=>$det
		);
		if($this->m_db->edit_row('paket',$d,$s)==TRUE)
		{
			return true;
		}else{
			return false;
		}
	}

	function paket_delete($pktID)
	{
		$s=array(
		'id_paket'=>$pktID,
		);
		
		if($this->m_db->delete_row('paket',$s)==TRUE)
		{
			return true;
		}else{
			return false;
		}
	}

	function faq_add($tny,$jwb)
	{
		$d=array(
		'judul'=>$tny,
		'isi'=>$jwb
		);

		if($this->m_db->add_row('faq',$d)==TRUE)
		{
			return true;
		}else{
			return false;
		}
	}

	function faq_edit($faqID,$tny,$jwb)
	{
		$s=array(
		'id_faq'=>$faqID,
		);
		$d=array(
		'judul'=>$tny,
		'isi'=>$jwb
		);
		if($this->m_db->edit_row('faq',$d,$s)==TRUE)
		{
			return true;
		}else{
			return false;
		}
	}

	function faq_delete($faqID)
	{
		$s=array(
		'id_faq'=>$faqID,
		);
		
		if($this->m_db->delete_row('faq',$s)==TRUE)
		{
			return true;
		}else{
			return false;
		}
	}

	function syarat_edit($konfigID,$isi)
	{
		$s=array(
		'konfigurasi_id'=>$konfigID,
		);
		$d=array(
		'isi'=>$isi
		);
		if($this->m_db->edit_row('konfigurasi',$d,$s)==TRUE)
		{
			return true;
		}else{
			return false;
		}
	}
}