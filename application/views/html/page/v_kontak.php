
  <!-- end header -->
  <section id="inner-headline">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <ul class="breadcrumb">
          <li><a href="#"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
          <li class="active">Kontak</li>
        </ul>
      </div>
    </div>
  </div>
  </section>
  <section id="content">
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
        <h4>Get in touch with us by filling <strong>contact form below</strong></h4>
        <form id="contactform" action="contact/contact.php" method="post" class="validateform" name="send-contact">
          <div id="sendmessage">
             Your message has been sent. Thank you!
          </div>
          <div class="row">
            <div class="col-lg-4 field">
              <input type="text" name="name" placeholder="* Enter your full name" data-rule="maxlen:4" data-msg="Please enter at least 4 chars" />
              <div class="validation">
              </div>
            </div>
            <div class="col-lg-4 field">
              <input type="text" name="email" placeholder="* Enter your email address" data-rule="email" data-msg="Please enter a valid email" />
              <div class="validation">
              </div>
            </div>
            <div class="col-lg-4 field">
              <input type="text" name="subject" placeholder="Enter your subject" data-rule="maxlen:4" data-msg="Please enter at least 4 chars" />
              <div class="validation">
              </div>
            </div>
            <div class="col-lg-12 margintop10 field">
              <textarea rows="12" name="message" class="input-block-level" placeholder="* Your message here..." data-rule="required" data-msg="Please write something"></textarea>
              <div class="validation">
              </div>
              <p>
                <button class="btn btn-theme margintop10 pull-left" type="submit">Submit message</button>
                <span class="pull-right margintop20">* Please fill all required form field, thanks!</span>
              </p>
            </div>
          </div>
        </form>
      </div>
      <div class="col-lg-4">
        <aside class="right-sidebar">
        <div class="widget">
          <form class="form-search">
            <input class="form-control" type="text" placeholder="Search..">
          </form>
        </div>
        <div class="widget">
          <h5 class="widgetheading">Categories</h5>
          <ul class="cat">
            <?php if (!empty($kat)) {
              foreach ($kat as $rows) { ?>
                <li><i class="icon-angle-right"></i><a href="#"><?=$rows->nama_kat?></a><span> (20)</span></li>
              <?php  
              }
            } ?>
            
          </ul>
        </div>
        <div class="widget">
          <h5 class="widgetheading">Latest posts</h5>
          <ul class="recent">
            <?php if (!empty($latest)) {
              foreach ($latest as $r) 
              { $cover=field_value('blog_cover','id_blog',$r->id_blog,'cover'); ?>
                <li>
                  <img src="http://bikinbuku.co.id/assets/images/blog/thumbs/64/<?=$cover;?>" class="pull-left" alt="" />
                  <h6><a href="<?php echo base_url()?>detail/blog/<?=$r->id_blog;?>/<?=$r->meta;?>"><?=$r->judul?></a></h6>
                  <?php echo cut_text($r->isi,100,3); ?>....</p>
                </li>
              <?php }
            } ?>
            
            
          </ul>
        </div>
        </aside>
      </div>
    </div>
  </div>
  </section>
  