
  <!-- end header -->
  <section id="inner-headline">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <ul class="breadcrumb">
          <li><a href="#"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
        </ul>
      </div>
    </div>
  </div>
  </section>
  <section id="content">
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
        <div id="postList">
          
        </div>
        <center style="text-align: center">
          <button class="btn" id="load_more">Load more..<img style="display: none" id="loader" src="<?php echo base_url();?>assets/loading.gif">
          </button>
          <input type="hidden" name="limit" id="limit" value="1"/>
        </center>
      </div>
      <div class="col-lg-4">
        <aside class="right-sidebar">
        <div class="widget">
          <form class="form-search">
            <input class="form-control" type="text" placeholder="Search..">
          </form>
        </div>
        <div class="widget">
          <h5 class="widgetheading">Kategori</h5>
          <ul class="cat">
            <?php if (!empty($kat)) {
              foreach ($kat as $rows)
              { $total = count($this->m_db->get_data('blog',array('id_kat_blog'=>$rows->id_kat_blog))); ?>
                <li><i class="icon-angle-right"></i><a href="#"><?=$rows->nama_kat?></a><span> (<?=$total;?>)</span></li>
              <?php  
              }
            } ?>
            
          </ul>
        </div>
        <div class="widget">
          <h5 class="widgetheading">Latest posts</h5>
          <ul class="recent">
            <?php if (!empty($latest)) {
              foreach ($latest as $r) 
              { $cover=field_value('blog_cover','id_blog',$r->id_blog,'cover'); ?>
                <li>
                  <img src="http://bikinbuku.co.id/assets/images/blog/thumbs/64/<?=$cover;?>" class="pull-left" alt="" />
                  <h6><a href="<?php echo base_url()?>detail/blog/<?=$r->id_blog;?>/<?=$r->meta;?>"><?=$r->judul?></a></h6>
                  <?php echo cut_text($r->isi,100,3); ?>....</p>
                </li>
              <?php }
            } ?>
            
            
          </ul>
        </div>
        </aside>
      </div>
    </div>
  </div>
  </section>
  <script type="text/javascript">
    $(document).ready(function(){
        getdata(0);
        $("#load_more").click(function(e){
            e.preventDefault();
            var page= $("input#limit").val();
            var num = +$("input#limit").val() + 1;
            $('input#limit').val(num);
            getdata(page);
            scroll();
        });
 
    });
 
    var getdata = function(page){
        $("#loader").show();
        $.ajax({
            url:"<?php echo base_url();?>beranda/get_data",
            type:'GET',
            data: {
              page:page
            },
        success: function(response){
            if (response) {
              $("#postList").append(response);
              $("#loader").hide();
            }
            else {
              $("#load_more").hide();
            }
          }
        });
    };
 
    var scroll  = function(){
        $('html, body').animate({
            scrollTop: $('#load_more').offset().top
        }, 1000);
    };
  </script>