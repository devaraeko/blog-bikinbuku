      <?php
      if(!empty($posts))
      {
        foreach($posts as $post)
        { $cov=field_value('blog_cover','id_blog',$post->id_blog,'cover'); ?>
        <article>
            <div class="post-image">
              <div class="post-heading">
                <h3><a href="#"><?=$post->judul?></a></h3>
              </div>
              <img src="http://localhost/bikinbuku/assets/images/blog/thumbs/400/<?=$cov;?>" alt="" />
            </div>
            <?php echo cut_text($post->isi,400,3); ?>....</p>
            <div class="bottom-article">
              <ul class="meta-post">
                <li><i class="icon-calendar"></i><a href="#"> <?=$post->tgl?></a></li>
                <li><i class="icon-user"></i><a href="#"> <?=$post->penulis?></a></li>
                <li><i class="icon-folder-open"></i><a href="#"> Blog</a></li>
              </ul>
              <a href="<?php echo base_url()?>detail/blog/<?=$post->id_blog;?>/<?=$post->meta;?>" class="pull-right">Selengkapnya <i class="icon-angle-right"></i></a>
            </div>
        </article>
      <?php } }
      else
      { ?>
        <p>Post(s) not available.</p>
      <?php } ?>
        <div>
          <center> <?php echo $this->ajax_pagination->create_links(); ?></center>
        </div>