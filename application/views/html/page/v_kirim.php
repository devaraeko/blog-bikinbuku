
  <!-- end header -->
  <section id="inner-headline">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <ul class="breadcrumb">
          <li><a href="#"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
          <li class="active">Kirim Tulisan</li>
        </ul>
      </div>
    </div>
  </div>
  </section>
  <section id="content">
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
      
      </div>
      <div class="col-lg-4">
        <aside class="right-sidebar">
        <div class="widget">
          <form class="form-search">
            <input class="form-control" type="text" placeholder="Search..">
          </form>
        </div>
        <div class="widget">
          <h5 class="widgetheading">Categories</h5>
          <ul class="cat">
            <?php if (!empty($kat)) {
              foreach ($kat as $rows) { ?>
                <li><i class="icon-angle-right"></i><a href="#"><?=$rows->nama_kat?></a><span> (20)</span></li>
              <?php  
              }
            } ?>
            
          </ul>
        </div>
        <div class="widget">
          <h5 class="widgetheading">Latest posts</h5>
          <ul class="recent">
            <?php if (!empty($latest)) {
              foreach ($latest as $r) 
              { $cover=field_value('blog_cover','id_blog',$r->id_blog,'cover'); ?>
                <li>
                  <img src="http://bikinbuku.co.id/assets/images/blog/thumbs/64/<?=$cover;?>" class="pull-left" alt="" />
                  <h6><a href="<?php echo base_url()?>detail/blog/<?=$r->id_blog;?>/<?=$r->meta;?>"><?=$r->judul?></a></h6>
                  <?php echo cut_text($r->isi,100,3); ?>....</p>
                </li>
              <?php }
            } ?>
            
            
          </ul>
        </div>
        </aside>
      </div>
    </div>
  </div>
  </section>