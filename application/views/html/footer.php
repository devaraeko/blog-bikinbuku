<footer>
  <div class="container">
    <div class="row">
      <div class="col-lg-3">
        <div class="widget">
          <h5 class="widgetheading">Get in touch with us</h5>
          <address>
          <strong><a href="http://bikinbuku.co.id" target="_blank">Bikinbuku.co.id</a></strong><br>
           Berbah Sleman<br>
           Yogyakarta Indonesia </address>
          <p>
            <i class="icon-phone"></i> (123) 456-7890 - (123) 555-7891 <br>
            <i class="icon-envelope-alt"></i> redaksi@bikinbuku.co.id
          </p>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="widget">
          <h5 class="widgetheading">Tulisan Terakhir</h5>
          <ul class="list-inline">
            <?php if (!empty($latest)) {
              foreach ($latest as $r) 
              { $cover=field_value('blog_cover','id_blog',$r->id_blog,'cover'); ?>
                <li>
                  <img src="http://bikinbuku.co.id/assets/images/blog/thumbs/64/<?=$cover;?>" class="pull-left" alt="" />
                  <a href="<?php echo base_url()?>baca/<?=$r->id_blog;?>/<?=$r->meta;?>" class="pull-right"><?=$r->judul?></a>
                </li>
              <?php }
            } ?>
          </ul>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="widget">
          <h5 class="widgetheading">Kategori</h5>
          <ul class="link-list">
            <?php if (!empty($kat)) {
              foreach ($kat as $rows)
              { $total = count($this->m_db->get_data('blog',array('id_kat_blog'=>$rows->id_kat_blog))); ?>
                <li><a href="#"><?=$rows->nama_kat?></a></li>
              <?php  
              }
            } ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div id="sub-footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-4">
          <div>
            <ul class="list-inline">
              <li><a href="<?php echo base_url();?>">Beranda</a></li>
              <li><a>|</a></li>
              <li><a href="<?php echo base_url();?>kirim">Kirim Tulisan</a></li>
              <li><a>|</a></li>
              <li><a href="<?php echo base_url();?>kontak">Kontak</a></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="copyright center">
            <p>
              <span>&copy; Blog Bikinbuku 2017 All right reserved. By </span><a href="http://bootstraptaste.com" target="_blank">Bootstraptaste</a>
            </p>
          </div>
        </div>
        <div class="col-lg-4">
          <ul class="social-network">
            <li><a href="#" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#" data-placement="top" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
            <li><a href="#" data-placement="top" title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
            <li><a href="#" data-placement="top" title="Google plus"><i class="fa fa-google-plus"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  </footer>
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>

  <script src="<?=base_url();?>assets/js/jquery.easing.1.3.js"></script>
  <script src="<?=base_url();?>assets/js/bootstrap.min.js"></script>
  <script src="<?=base_url();?>assets/js/jquery.fancybox.pack.js"></script>
  <script src="<?=base_url();?>assets/js/jquery.fancybox-media.js"></script>
  <script src="<?=base_url();?>assets/js/google-code-prettify/prettify.js"></script>
  <script src="<?=base_url();?>assets/js/jquery.flexslider.js"></script>
  <script src="<?=base_url();?>assets/js/animate.js"></script>
  <script src="<?=base_url();?>assets/js/custom.js"></script>
</body>
</html>