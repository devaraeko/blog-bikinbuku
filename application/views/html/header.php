<!DOCTYPE html>
<html lang="id-ID">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Bikinbuku.co.id atau Bikin Buku adalah sebuah perusahaan self-publishing berbasis online di Indonesia.">
  <meta name="keywords" content="self publishing, self publish, print on demand, bikin buku, bikinbuku.co.id, penerbit, penulis, buku">
  <meta name="robots" content="index follow">
  <meta nama='geo.placename' content="Indonesia">

  <meta property="og:title" content="<?=$title;?>" />
  <meta property="og:description" content="<?=$desc;?>" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="<?=$url;?>" />
  <meta property="og:site_name" content="Blog Bikinbuku.co.id"/>
  <meta property="og:image" content="<?=$img;?>" />
	<title><?=$judul;?></title>
  <link rel="shortcut icon" href="http://bikinbuku.co.id/assets/images/favicon.png">
  <link href="<?=base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?=base_url();?>assets/css/jcarousel.css" rel="stylesheet" />
  <link href="<?=base_url();?>assets/css/flexslider.css" rel="stylesheet" />
  <link href="<?=base_url();?>assets/css/bootstrap-social.css" rel="stylesheet">
  <link href="<?=base_url();?>assets/css/style.css" rel="stylesheet" />


  <!-- Theme skin -->
  <link href="<?=base_url();?>assets/skins/default.css" rel="stylesheet" />
  <script src="<?=base_url();?>assets/js/jquery.js"></script>
</head>
<body>
<div id="wrapper">
  <!-- start header -->
  <header>
        <div class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><img src="http://bikinbuku.co.id/assets/images/logo.png" class="img-responsive"></a>
                </div>
                <div class="navbar-collapse collapse ">
                    <ul class="nav navbar-nav">
                        <li><a href="<?php echo base_url();?>">Beranda</a></li>
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Kategori <b class=" icon-angle-down"></b></a>
                          <ul class="dropdown-menu">
                            <?php if (!empty($kat)) {
                              foreach ($kat as $rows)
                              { $id_kat = $rows->id_kat_blog; ?>
                                <li><a href="<?=base_url();?>kategori/<?=$id_kat;?>/<?=$rows->nama_kat?>"><?=$rows->nama_kat?></a></li>
                              <?php  
                              }
                            } ?>
                          </ul>
                        </li>
                        <li><a href="<?php echo base_url();?>kirim">Kirim Tulisan</a></li>
                        <li><a href="<?php echo base_url();?>kontak">Kontak</a></li>
                    </ul>
                </div>
            </div>
        </div>
  </header>
  