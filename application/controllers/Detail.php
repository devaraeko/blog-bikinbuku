<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detail extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('m_db');
	}

	function index($id)
	{
		$blogID = $id;
		$meta['judul'] = baca_konfig('bikin-buku')." - Detail";
		$d['blog']=$this->m_db->get_data('blog',array('id_blog'=>$blogID));
		$d['kat'] = $this->m_db->get_data('blog_kategori');
        $d['latest'] = $this->m_db->get_data('blog',array('sematkan'=>0),'tgl DESC','',3);
		$this->load->view('html/header',$meta);
		$this->load->view('html/page/v_detail',$d);
		$this->load->view('html/footer',$meta);
	}

	function blog($id)
	{
		$blogID = $id;
		$judul = field_value('blog','id_blog',$blogID,'judul');
		$meta['judul'] = baca_konfig('bikin-buku')." - ".$judul;
		$meta['title'] = $judul." | ".baca_konfig('bikin-buku');
		$meta['desc'] = cut_text(field_value('blog','id_blog',$blogID,'isi'),100,3);
		$meta['url'] = "http://localhost/blog-bikinbuku/detail/".$blogID."/".field_value('blog','id_blog',$blogID,'meta');
		$meta['img'] = "http://localhost/bikinbuku/assets/images/blog/thumbs/400/".field_value('blog_cover','id_blog',$blogID,'cover');

		$d['blog']=$this->m_db->get_data('blog',array('id_blog'=>$blogID));
		$d['cumb']=field_value('blog','id_blog',$blogID,'judul');
		$d['kat'] = $this->m_db->get_data('blog_kategori');
        $d['latest'] = $this->m_db->get_data('blog',array('sematkan'=>0),'tgl DESC','',3);
		$this->load->view('html/header',$meta);
		$this->load->view('html/page/v_detail',$d);
		$this->load->view('html/footer',$meta);
	}

	function view($meta)
	{
		$blogID = field_value('blog','meta',$meta,'id_blog');
		$judul = field_value('blog','id_blog',$blogID,'judul');
		$meta['judul'] = baca_konfig('bikin-buku')." - ".$judul;
		$meta['title'] = $judul." | ".baca_konfig('bikin-buku');
		$meta['desc'] = cut_text(field_value('blog','id_blog',$blogID,'isi'),100,3);
		$meta['url'] = "http://localhost/blog-bikinbuku/detail/".$blogID."/".field_value('blog','id_blog',$blogID,'meta');
		$meta['img'] = "http://localhost/bikinbuku/assets/images/blog/thumbs/400/".field_value('blog_cover','id_blog',$blogID,'cover');
		$d['blog']=$this->m_db->get_data('blog',array('id_blog'=>$blogID));
		$d['cumb']=field_value('blog','id_blog',$blogID,'judul');
		$d['kat'] = $this->m_db->get_data('blog_kategori');
        $d['latest'] = $this->m_db->get_data('blog',array('sematkan'=>0),'tgl DESC','',3);
        $this->load->view('html/header',$meta);
		$this->load->view('html/page/v_detail',$d);
		$this->load->view('html/footer',$meta);
	}

}