<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Kategori extends CI_Controller
{
	
	function __construct()
    {
        parent::__construct();
        $this->load->library('m_db');
        $this->load->model('load_model');
    }

    function index()
    {
    	
    }

    function kat($id)
    {
    	$IDkat = $id;
    	$kate = field_value('blog_kategori','id_kat_blog',$IDkat,'nama_kat');
    	$meta['judul'] = "Blog ".baca_konfig('bikin-buku')." - ".$kate;
        $meta['title'] = "Blog | ".baca_konfig('bikin-buku');
        $meta['desc'] = baca_konfig('deskripsi-web');
        $meta['url'] = "http://blog.bikinbuku.co.id/";
        $meta['img'] = "http://bikinbuku.co.id/assets/images/logo.png";
        $meta['bnr']=$this->m_db->get_data('blog',array('banner'=>1));
        $meta['kat'] = $this->m_db->get_data('blog_kategori');
        $this->load->view('html/header',$meta);
        $data['idKat'] = $id;
        $data['namaKat'] = field_value('blog_kategori','id_kat_blog',$IDkat,'nama_kat');
        $data['semat'] = $this->m_db->get_data('blog',array('sematkan'=>1));
        $data['kat'] = $this->m_db->get_data('blog_kategori');
        $data['latest'] = $this->m_db->get_data('blog',array('sematkan'=>0),'tgl DESC','',3);
        $this->load->view('html/page/v_kat',$data);
        $this->load->view('html/footer',$data);
    }

    function get_data()
    {
        $page = $this->input->get('page');
        $kategori = $this->input->get('kat');
        $totalRec = count($this->m_db->get_data('blog',array('id_kat_blog'=>$kategori)));
        if ($page==$totalRec) {
            exit;
        }
        else {
            $posts = $this->load_model->getdataKat(array('id_kat_blog'=>$kategori),$page);
            foreach ($posts as $post) {
                $cov=field_value('blog_cover','id_blog',$post->id_blog,'cover');
                $kate=field_value('blog_kategori','id_kat_blog',$post->id_kat_blog,'nama_kat');
                echo "
                    <article>
                        <div class='post-image'>
                          <div class='post-heading'>
                            <h3><a href='#'>".$post->judul."</a></h3>
                          </div>
                          <img src='http://bikinbuku.co.id/assets/images/blog/thumbs/400/".$cov."' alt='' />
                        </div>
                        ".cut_text($post->isi,400,3)."...</p>
                        <div class='bottom-article'>
                          <ul class='meta-post'>
                            <li><i class='icon-calendar'></i><a href='#'> ".$post->tgl."</a></li>
                            <li><i class='icon-user'></i><a href='#'> ".$post->penulis."</a></li>
                            <li><i class='icon-folder-open'></i><a href='#'> ".$kate."</a></li>
                          </ul>
                          <a href='".base_url()."baca/".$post->id_blog."/".$post->meta."' class='pull-right'>Selengkapnya <i class='icon-angle-right'></i></a>
                        </div>
                    </article>
                ";
            }
        }
    }
}