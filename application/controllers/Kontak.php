<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kontak extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('m_db');
	}

	function index()
	{
		
		$meta['judul'] = baca_konfig('bikin-buku')." - Kontak";
		$meta['title'] = "Blog | ".baca_konfig('bikin-buku');
        $meta['desc'] = baca_konfig('deskripsi-web');
        $meta['url'] = "http://localhost/blog-bikinbuku/";
        $meta['img'] = "http://localhost/bikinbuku/assets/images/logo.png";
		$d['kat'] = $this->m_db->get_data('blog_kategori');
        $d['latest'] = $this->m_db->get_data('blog',array('sematkan'=>0),'tgl DESC','',3);
		$this->load->view('html/header',$meta);
		$this->load->view('html/page/v_kontak',$d);
		$this->load->view('html/footer',$meta);
	}
}