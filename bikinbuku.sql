-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 20, 2016 at 07:01 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bikinbuku`
--

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE IF NOT EXISTS `bank` (
  `bank_id` int(11) NOT NULL,
  `nama_bank` varchar(100) NOT NULL,
  `pemilik` varchar(100) NOT NULL,
  `no_rek` varchar(100) NOT NULL,
  `logo` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE IF NOT EXISTS `blog` (
  `id_blog` int(11) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `meta` varchar(100) NOT NULL,
  `id_kat_blog` int(11) NOT NULL,
  `isi` text NOT NULL,
  `penulis` varchar(30) NOT NULL,
  `tgl` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `hits` int(11) DEFAULT NULL,
  `banner` int(11) DEFAULT '0',
  `sematkan` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id_blog`, `judul`, `meta`, `id_kat_blog`, `isi`, `penulis`, `tgl`, `hits`, `banner`, `sematkan`) VALUES
(2, 'Masak Yuk', 'masak-yuk', 1, '<p>Lorem Ipsum adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting. Lorem Ipsum telah menjadi standar contoh teks sejak tahun 1500an, saat seorang tukang cetak yang tidak dikenal mengambil sebuah kumpulan teks dan mengacaknya untuk menjadi sebuah buku contoh huruf. Ia tidak hanya bertahan selama 5 abad, tapi juga telah beralih ke penataan huruf elektronik, tanpa ada perubahan apapun. Ia mulai dipopulerkan pada tahun 1960 dengan diluncurkannya lembaran-lembaran Letraset yang menggunakan kalimat kalimat dari Lorem Ipsum, dan seiring munculnya perangkat lunak Desktop Publishing seperti Aldus PageMaker juga memiliki versi Lorem Ipsum.</p>\r\n', 'Admin Bikinbuku', '2016-11-20 16:24:53', NULL, 1, 0),
(3, 'Syarat dan Ketentuan di Bikinbuku.co.id', 'syarat-ketentuan-bikinbuku', 0, '<p>Syarat & Ketentuan:</p>\r\n\r\n<p>Bikinbuku.co (atau selanjutnya disebut “bikinbuku”, “kami”) adalah komunitas penulis, pendidik, ataupun masyarakat umum yang peduli dengan budaya literasi Indonesia, pegiat buku, ataupun pekarya yang ingin mengabadikan karyanya agar tidak hilang ditelan zaman. Kami menyediakan berbagai fasilitas publikasi yang memudahkan Anda membuat dan memasarkan buku dengan pelayanan yang cepat dan berkualitas. Syarat dan ketentuan yang kami buat tidak hanya melindungi karya dan privasi Anda, namun juga sebagai komitmen kami untuk memberikan layanan terbaik.</p>\r\n\r\n<p>Bikinbuku.co adalah wadah di mana masyarakat segala umur, latar belakang, pengalaman, dan pekerjaan dapat membuat buku dan menjualnya. Kami membantu masyarkat untuk menyebarluaskan gagasannya.</p>\r\n\r\n<p>Sebelum Anda mendaftar keanggotaan dalam situs kami, silakan baca syarat dan ketentuan di bawah ini:</p>\r\n\r\n<ul xss=removed>\r\n <li>Menjadi anggota bikinbuku.co tidak dipungut biaya apapun atau gratis.</li>\r\n <li>Anggota bikinbuku.co terbuka bagi seluruh lapisan masyarakat yang ingin membukukan karyanya.</li>\r\n <li>Seorang anggota hanya diperkenankan memiliki 1 (satu) akun.</li>\r\n <li>Naskah yang Anda unggah ke database kami merupakan karya original, bukan terjemahan, bukan saduran dari karya orang lain.</li>\r\n <li>Karya yang Anda unggah tidak menyinggung SARA, agama, dan golongan tertentu, serta tidak mengandung unsur pornografi.</li>\r\n <li>Hak cipta naskah tetap melekat pada Anda.</li>\r\n <li>Jika kelak muncul gugatan hukum dari pihak lain atas orisinalitas naskah Anda, maka segala masalah hukum yang muncul sepenuhnya menjadi tanggung jawab Anda.</li>\r\n</ul>\r\n\r\n<p>Kami mungkin menambah atau mengurangi syarat dan ketentuan yang kami jabarkan di atas. Namun, segera mungkin akan kami beritahukan kepada Anda melalui surel ataupun <em>banner</em> pengumuman yang akan kami sematkan di situs ini.</p>\r\n', 'Nichal Zaki', '2016-11-19 21:10:35', NULL, 1, 0),
(4, 'Framework PHP yang Populer', 'framework-php-yang-populer', 0, '<p>Lorem Ipsum adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting. Lorem Ipsum telah menjadi standar contoh teks sejak tahun 1500an, saat seorang tukang cetak yang tidak dikenal mengambil sebuah kumpulan teks dan mengacaknya untuk menjadi sebuah buku contoh huruf. Ia tidak hanya bertahan selama 5 abad, tapi juga telah beralih ke penataan huruf elektronik, tanpa ada perubahan apapun. Ia mulai dipopulerkan pada tahun 1960 dengan diluncurkannya lembaran-lembaran Letraset yang menggunakan kalimat kalimat dari Lorem Ipsum, dan seiring munculnya perangkat lunak Desktop Publishing seperti Aldus PageMaker juga memiliki versi Lorem Ipsum.</p>\r\n', 'Nical Zaki', '2016-11-19 21:10:35', NULL, 0, 0),
(5, 'Mengenal Laravel', 'mengenal-laravel', 0, '<p>Lorem Ipsum adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting. Lorem Ipsum telah menjadi standar contoh teks sejak tahun 1500an, saat seorang tukang cetak yang tidak dikenal mengambil sebuah kumpulan teks dan mengacaknya untuk menjadi sebuah buku contoh huruf. Ia tidak hanya bertahan selama 5 abad, tapi juga telah beralih ke penataan huruf elektronik, tanpa ada perubahan apapun. Ia mulai dipopulerkan pada tahun 1960 dengan diluncurkannya lembaran-lembaran Letraset yang menggunakan kalimat kalimat dari Lorem Ipsum, dan seiring munculnya perangkat lunak Desktop Publishing seperti Aldus PageMaker juga memiliki versi Lorem Ipsum.</p>\r\n', 'Devara Eko', '2016-11-19 21:10:35', NULL, 0, 1),
(6, 'Cake PHP masih menjadi Pilihan', 'cake-php-masih-menjadi-pilihan', 0, '<p>Lorem Ipsum adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting. Lorem Ipsum telah menjadi standar contoh teks sejak tahun 1500an, saat seorang tukang cetak yang tidak dikenal mengambil sebuah kumpulan teks dan mengacaknya untuk menjadi sebuah buku contoh huruf. Ia tidak hanya bertahan selama 5 abad, tapi juga telah beralih ke penataan huruf elektronik, tanpa ada perubahan apapun. Ia mulai dipopulerkan pada tahun 1960 dengan diluncurkannya lembaran-lembaran Letraset yang menggunakan kalimat kalimat dari Lorem Ipsum, dan seiring munculnya perangkat lunak Desktop Publishing seperti Aldus PageMaker juga memiliki versi Lorem Ipsum.</p>\r\n', 'Sartono', '2016-11-19 21:10:35', NULL, 1, 0),
(7, 'Ngopri (Ngobrol Ngecipris)', 'ngopri-ngobrol-ngecipris', 0, '<p>Lorem Ipsum adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting. Lorem Ipsum telah menjadi standar contoh teks sejak tahun 1500an, saat seorang tukang cetak yang tidak dikenal mengambil sebuah kumpulan teks dan mengacaknya untuk menjadi sebuah buku contoh huruf. Ia tidak hanya bertahan selama 5 abad, tapi juga telah beralih ke penataan huruf elektronik, tanpa ada perubahan apapun. Ia mulai dipopulerkan pada tahun 1960 dengan diluncurkannya lembaran-lembaran Letraset yang menggunakan kalimat kalimat dari Lorem Ipsum, dan seiring munculnya perangkat lunak Desktop Publishing seperti Aldus PageMaker juga memiliki versi Lorem Ipsum.</p>\r\n', 'Fahrizal Yusuf', '2016-11-19 21:10:35', NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `blog_cover`
--

CREATE TABLE IF NOT EXISTS `blog_cover` (
  `id_block_cover` int(11) NOT NULL,
  `id_blog` int(11) NOT NULL,
  `cover` varchar(50) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_cover`
--

INSERT INTO `blog_cover` (`id_block_cover`, `id_blog`, `cover`) VALUES
(1, 1, 'blog_1-1.png'),
(15, 2, 'blog_2-1.png'),
(8, 3, 'blog_3-1.png'),
(12, 4, 'blog_4-1.jpg'),
(10, 5, 'blog_5-1.jpg'),
(13, 6, 'blog_6-1.png'),
(9, 7, 'blog_7-1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `blog_kategori`
--

CREATE TABLE IF NOT EXISTS `blog_kategori` (
  `id_kat_blog` int(11) NOT NULL,
  `nama_kat` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_kategori`
--

INSERT INTO `blog_kategori` (`id_kat_blog`, `nama_kat`) VALUES
(1, 'Bisnis'),
(2, 'Teknologi'),
(3, 'Sastra'),
(6, 'Politik');

-- --------------------------------------------------------

--
-- Table structure for table `buku`
--

CREATE TABLE IF NOT EXISTS `buku` (
  `id_buku` int(11) NOT NULL,
  `kat_id` int(11) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `member_id` int(11) NOT NULL,
  `penerbit` varchar(30) NOT NULL,
  `ISBN` varchar(20) NOT NULL,
  `tgl_terbit` date NOT NULL,
  `harga` double NOT NULL,
  `ukuran` varchar(15) NOT NULL,
  `jml_halaman` int(11) NOT NULL,
  `sinopsis` text NOT NULL,
  `status` varchar(20) DEFAULT NULL,
  `hits` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buku`
--

INSERT INTO `buku` (`id_buku`, `kat_id`, `judul`, `member_id`, `penerbit`, `ISBN`, `tgl_terbit`, `harga`, `ukuran`, `jml_halaman`, `sinopsis`, `status`, `hits`) VALUES
(2, 25, 'Intisari Kultum', 2, 'Penerbit Genesis', '23425', '2016-11-14', 50000, '11 x 18 cm', 125, '<p>-</p>\r\n', NULL, NULL),
(3, 25, 'Intisari Khutbah', 2, 'Penerbit Genesis', '256242', '2016-11-02', 50000, '11 x 18 cm', 145, '<p>-</p>\r\n', NULL, NULL),
(4, 7, 'Undang Undang Pilkada', 2, 'Penerbit Genesis', '2652423123', '2016-10-04', 45000, '10 x 17 cm', 200, '<p>-</p>\r\n', NULL, NULL),
(5, 4, 'Atlas Indonesia', 2, 'Penerbit Genesis', '2524131312', '2016-08-02', 75000, '13 x 21 cm', 150, '<p>-</p>\r\n', NULL, NULL),
(6, 18, 'Modul Tes TPA', 2, 'Penerbit Genesis', '2542511313', '2016-03-15', 120000, '12 x 19 cm', 420, '<p>Modul tes TPA yang..</p>\r\n', NULL, NULL),
(7, 21, 'Pedoman EYD', 2, 'Penerbit Genesis', '252625412', '2016-05-02', 65000, '12 x 18 cm', 145, '<p>-</p>\r\n', NULL, NULL),
(8, 25, 'Tuntunan Shalat', 2, 'Penerbit Genesis', '123523625', '2016-07-05', 40000, '10 x 18 cm', 80, '<p>-</p>\r\n', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `buku_cover`
--

CREATE TABLE IF NOT EXISTS `buku_cover` (
  `id_cover` int(11) NOT NULL,
  `id_buku` int(11) NOT NULL,
  `cover` varchar(100) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buku_cover`
--

INSERT INTO `buku_cover` (`id_cover`, `id_buku`, `cover`) VALUES
(10, 3, 'buku_3-1.png'),
(9, 2, 'buku_2-1.png'),
(11, 4, 'buku_4-1.jpg'),
(12, 5, 'buku_5-1.jpg'),
(13, 6, 'buku_6-1.png'),
(14, 7, 'buku_7-1.png'),
(15, 8, 'buku_8-1.png');

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE IF NOT EXISTS `faq` (
  `id_faq` int(11) NOT NULL,
  `judul` varchar(200) NOT NULL,
  `isi` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`id_faq`, `judul`, `isi`) VALUES
(1, 'Apakah bikinbuku.co.id adalah penerbit?', '<p>Bikinbuku.co.id BUKAN penerbit. Kami adalah mitra Anda atau para penulis lainnya untuk menjadi penerbit sendiri (Self Publishing). Dengan demikian sebenarnya yang menjadi penerbit adalah si penulis itu sendiri, dan tentunya <strong>penulis juga masih memiliki hak cipta atas karyanya, dan</strong><strong> tentu saja penulis  boleh menawarkan karyanya ke pihak lain/ ke penerbit lain dengan bebas.</strong></p>\r\n'),
(2, 'Apa artinya selfpublishing?', '<p>Selfpublishing, dikenal juga sebagai Penerbitan Mandiri, adalah salah satu cara menerbitkan buku oleh penulis tanpa bantuan penerbit mayor (Major Publisher), a<strong>rtinya penulis juga merangkap tugas sebagai penerbit yang bertanggung jawab penuh atas keseluruhan proses penerbitan bukunya, dari mulai menulis naskah, desain sampul buku dan lay-out naskah, menentukan harga buku, distribusi sampai dengan memasarkan buku.</strong> Jika penulis tidak dapat membuat desain sendiri untuk cover dan lay-out isi naskahnya, kebanyakan mereka menggunakan jasa desain dari orang lain.</p>\r\n'),
(3, 'Apakah artinya Self Publishing - Print On Demand?', '<p>Print On demand adalah sistem yang digunakan oleh bikinbuku,co,id, artinya kami mencetak buku hanya berdasarkan pesanan saja. Jadi kami tidak mempunyai stok di gudang karena kami hanya mencetak sejumlah pesanan saja. Karena itu dalam setiap pesanan kami membutuhkan beberapa hari untuk proses cetak, post-press dan pengiriman, rata-rata waktu yang kami butuhkan untuk proses tersebut adalah 10 hari kerja (kecuali Sabtu-Minggu, dan hari libur tidak termasuk)</p>\r\n'),
(4, 'Bagaimana cara menerbitkan buku di bikinbuku.co.id?', '<p>Untuk melakukan pengunggahan naskah, Anda perlu mendaftarkan diri Anda terlebih dahulu sebagai member. Anda diharuskan menyertakan email asli Anda guna validasi atau verifikasi nantinya. Setelah akun Anda diaktifkan, maka silakan login menggunakan username dan password Anda. Setelah itu lakukan pengunggahan naskah melewati web bikinbuku.co.id ini.</p>'),
(5, 'Bagaimana Cara Pemasaran di Bikinbuku.co.id', '<p>Kami melakukan marketing atau pemasaran secara online dengan memanfaatkan sosial media, seperti Facebook, Twiiter, Instagram dan juga melalui website bikinbuku.co.id</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `file_project`
--

CREATE TABLE IF NOT EXISTS `file_project` (
  `id_file` int(11) NOT NULL,
  `id_project` int(11) NOT NULL,
  `naskah` varchar(150) NOT NULL,
  `cover` varchar(50) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `file_project`
--

INSERT INTO `file_project` (`id_file`, `id_project`, `naskah`, `cover`) VALUES
(2, 4, 'Template-Naskah.doc', NULL),
(3, 5, 'ASWAJA_SEBAGAI_MANHAJ_AL.docx', NULL),
(4, 6, 'FORMULIR_Riwayat_Kesehatan_Lomba.docx', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kat_buku`
--

CREATE TABLE IF NOT EXISTS `kat_buku` (
  `kat_id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kat_buku`
--

INSERT INTO `kat_buku` (`kat_id`, `nama`) VALUES
(2, 'Anak-anak'),
(3, 'Bahasa'),
(4, 'Biografi'),
(5, 'Ekonomi'),
(6, 'Hobi dan Usaha'),
(7, 'Hukum dan Perundangan'),
(8, 'Humor'),
(9, 'Kesehatan'),
(10, 'Komik'),
(11, 'Komputer dan Internet'),
(12, 'Kumpulan Cerpen'),
(13, 'Kumpulan Puisi'),
(14, 'Manajemen dan Bisnis'),
(15, 'Novel'),
(16, 'Keluarga'),
(17, 'Pariwisata'),
(18, 'Pendidikan'),
(19, 'Psikologi'),
(20, 'Sains dan Teknologi'),
(21, 'Sastra'),
(22, 'Sejarah dan Budaya'),
(23, 'Sosial Politik'),
(24, 'True Story'),
(25, 'Agama Islam');

-- --------------------------------------------------------

--
-- Table structure for table `kertas`
--

CREATE TABLE IF NOT EXISTS `kertas` (
  `id_kertas` int(11) NOT NULL,
  `nama_kertas` varchar(30) NOT NULL,
  `hitamputih` int(11) NOT NULL,
  `warna` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kertas`
--

INSERT INTO `kertas` (`id_kertas`, `nama_kertas`, `hitamputih`, `warna`) VALUES
(1, 'HVS-12x19cm', 75, 900),
(2, 'Bookpaper-13x19cm', 85, 900),
(3, 'HVS-14.5x20.5cm', 80, 950),
(4, 'Bookpaper-14.5x20.5cm', 90, 1000);

-- --------------------------------------------------------

--
-- Table structure for table `konfigurasi`
--

CREATE TABLE IF NOT EXISTS `konfigurasi` (
  `konfigurasi_id` int(11) NOT NULL,
  `nama_key` varchar(50) NOT NULL,
  `isi` text NOT NULL,
  `tipe` varchar(15) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konfigurasi`
--

INSERT INTO `konfigurasi` (`konfigurasi_id`, `nama_key`, `isi`, `tipe`) VALUES
(1, 'nama-aplikasi', 'bikinbuku.co.id', 'umum'),
(2, 'bikin-buku', 'Bikin Buku', 'umum'),
(3, 'company-address', 'Jalan Permindo No. 61 A', 'umum'),
(4, 'company-phone', '0751-21443', 'umum'),
(5, 'company-email', 'redaksi.bikinbuku@gmail.com', 'umum'),
(6, 'tema-aktif', 'lte', 'tema'),
(7, 'tema-logo', 'logo.png', 'tema'),
(8, 'syarat', '<p>Bikinbuku.co.id (atau selanjutnya disebut “bikinbuku”, “kami”) adalah komunitas penulis, pendidik, ataupun masyarakat umum yang peduli dengan budaya literasi Indonesia, pegiat buku, ataupun pekarya yang ingin mengabadikan karyanya agar tidak hilang ditelan zaman. Kami menyediakan berbagai fasilitas publikasi yang memudahkan Anda membuat dan memasarkan buku dengan pelayanan yang cepat dan berkualitas. Syarat dan ketentuan yang kami buat tidak hanya melindungi karya dan privasi Anda, namun juga sebagai komitmen kami untuk memberikan layanan terbaik.</p>\r\n\r\n<p>Bikinbuku.co adalah wadah di mana masyarakat segala umur, latar belakang, pengalaman, dan pekerjaan dapat membuat buku dan menjualnya. Kami membantu masyarkat untuk menyebarluaskan gagasannya.</p>\r\n\r\n<p>Sebelum Anda mendaftar keanggotaan dalam situs kami, silakan baca syarat dan ketentuan di bawah ini:</p>\r\n\r\n<ol>\r\n <li>Menjadi anggota bikinbuku.co.id tidak dipungut biaya apapun atau gratis.</li>\r\n <li>Anggota bikinbuku.co.id terbuka bagi seluruh lapisan masyarakat yang ingin membukukan karyanya.</li>\r\n <li>Seorang anggota hanya diperkenankan memiliki 1 (satu) akun. Untuk pendaftaran <a href="http://localhost/bikinbuku/member/daftar" target="_blank">disini</a>.</li>\r\n <li>Naskah yang Anda unggah ke database kami merupakan karya original, bukan terjemahan, bukan saduran dari karya orang lain.</li>\r\n <li>Karya yang Anda unggah tidak menyinggung SARA, agama, dan golongan tertentu, serta tidak mengandung unsur pornografi.</li>\r\n <li>Hak cipta naskah tetap melekat pada Anda.</li>\r\n <li>Jika kelak muncul gugatan hukum dari pihak lain atas orisinalitas naskah Anda, maka segala masalah hukum yang muncul sepenuhnya menjadi tanggung jawab Anda.</li>\r\n</ol>\r\n\r\n<p>Kami mungkin menambah atau mengurangi syarat dan ketentuan yang kami jabarkan di atas. Namun, segera mungkin akan kami beritahukan kepada Anda melalui surel ataupun banner pengumuman yang akan kami sematkan di situs ini.</p>\r\n', 'umum'),
(9, 'deskripsi-web', 'Blog Bikinbuku.co.id | Self Publishing in Indonesia', 'umum');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `id` int(11) NOT NULL,
  `email` varchar(30) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jk` varchar(15) NOT NULL,
  `telp` varchar(15) NOT NULL,
  `fb` varchar(30) NOT NULL,
  `twitter` varchar(30) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `nama_bank` varchar(30) DEFAULT NULL,
  `kode_bank` int(15) DEFAULT NULL,
  `no_rek` varchar(30) DEFAULT NULL,
  `nama_rek` varchar(30) DEFAULT NULL,
  `terakhir_login` datetime DEFAULT CURRENT_TIMESTAMP,
  `aktif` int(11) DEFAULT NULL,
  `token` varchar(50) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id`, `email`, `nama`, `username`, `password`, `tgl_lahir`, `jk`, `telp`, `fb`, `twitter`, `alamat`, `nama_bank`, `kode_bank`, `no_rek`, `nama_rek`, `terakhir_login`, `aktif`, `token`) VALUES
(2, 'devaraekokm@gmail.com', 'Devara Eko Katon Mahardika', 'devaraeko', '359a628dfe2c93abd1fafcaef959a0c9', '2016-10-03', 'laki-laki', '085643756855', 'Devara Eko', '@devarakowoll', 'Maguwoharjo Depok Sleman YK', 'BNI', 101, '423525255', 'Devara Eko', '2016-11-20 16:06:43', 1, 'c81e728d9d4c2f636f067f89cc14862c'),
(4, 'devara_kowoll@yahoo.com', 'Katon Mahardika', 'mahardika', '7238500f05f9fa38d09ae8318d188080', '1994-09-07', 'laki-laki', '089123788090', 'Katon Mahardika', '@katon_mahardika', 'Sleman DIY', 'BCA', 98, '09832787821387', 'Katon Mahardika', '2016-11-12 01:05:31', 1, 'a87ff679a2f3e71d9181a67b7542122c'),
(5, 'diendalorabuana@gmail.com', 'Dienda Lora Buana', 'diendalora', 'd51b621a876c3e0c65b25b8a4107e1f7', '1994-10-10', 'laki-laki', '085768944009', 'Dienda Lora Buana', '@diendalora', 'Brebes', 'BRI', 9, '899023825325', 'Dienda Lora Buana', '2016-11-13 17:35:58', 1, NULL),
(13, 'kotakpos680@gmail.com', 'Square Internet Cafe', 'squarenet', '359a628dfe2c93abd1fafcaef959a0c9', '2016-11-01', 'laki-laki', '085643756855', 'Square Net', '@squarenet', 'Jakal km 6,8', 'BNI', 768, '87893532428', 'Square Internet', '2016-11-18 18:40:16', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `paket`
--

CREATE TABLE IF NOT EXISTS `paket` (
  `id_paket` int(11) NOT NULL,
  `nama_paket` varchar(50) NOT NULL,
  `harga` int(11) NOT NULL,
  `fitur` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paket`
--

INSERT INTO `paket` (`id_paket`, `nama_paket`, `harga`, `fitur`) VALUES
(1, 'Guru/Dosen', 300000, 'Belum ada'),
(2, 'Pejabat/PNS', 500000, 'Belum ada'),
(3, 'Pengusaha/Profesional', 450000, 'Belum ada'),
(4, 'Pelajar/Mahasiswa', 250000, 'Belum ada'),
(5, 'Penerbit/Nirredaksi', 450000, 'Belum ada');

-- --------------------------------------------------------

--
-- Table structure for table `paket_custom`
--

CREATE TABLE IF NOT EXISTS `paket_custom` (
  `id_pkt_custom` int(11) NOT NULL,
  `id_project` int(11) NOT NULL,
  `penulisan` int(11) DEFAULT NULL,
  `penyuntingan` int(11) DEFAULT NULL,
  `alihbahasa` int(11) DEFAULT NULL,
  `sampul` int(11) DEFAULT NULL,
  `tataletak` int(11) DEFAULT NULL,
  `ilustrasi` int(11) DEFAULT NULL,
  `isbn` int(11) DEFAULT NULL,
  `cetak` int(11) DEFAULT NULL,
  `pemasaran` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paket_custom`
--

INSERT INTO `paket_custom` (`id_pkt_custom`, `id_project`, `penulisan`, `penyuntingan`, `alihbahasa`, `sampul`, `tataletak`, `ilustrasi`, `isbn`, `cetak`, `pemasaran`) VALUES
(1, 6, NULL, 1, 1, NULL, 1, NULL, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `id_project` int(11) NOT NULL,
  `kode_order` varchar(50) NOT NULL,
  `member_id` int(11) NOT NULL,
  `id_paket` int(11) NOT NULL,
  `tgl_order` date NOT NULL,
  `total` int(11) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id_project`, `kode_order`, `member_id`, `id_paket`, `tgl_order`, `total`, `status`) VALUES
(1, '1478109286', 2, 4, '2016-11-02', 692500, 'menunggu'),
(3, '1479036180', 2, 4, '2016-11-13', 760000, 'menunggu'),
(4, '1479244604', 2, 4, '2016-11-15', 632500, 'menunggu'),
(5, '1479560579', 2, 4, '2016-11-19', 632500, 'draft'),
(6, '1479604564', 2, 10, '2016-11-20', 530000, 'menunggu');

-- --------------------------------------------------------

--
-- Table structure for table `project_detail`
--

CREATE TABLE IF NOT EXISTS `project_detail` (
  `detail_id` int(11) NOT NULL,
  `id_project` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `penulis` varchar(50) NOT NULL,
  `penerbit` varchar(50) NOT NULL,
  `kat_id` varchar(50) NOT NULL,
  `id_kertas` int(11) NOT NULL,
  `jml_hitam` int(11) NOT NULL,
  `jml_warna` int(11) NOT NULL,
  `harga_satuan` int(11) NOT NULL,
  `harga_konsumen` int(11) NOT NULL,
  `jml_cetak` int(11) NOT NULL,
  `catatan` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_detail`
--

INSERT INTO `project_detail` (`detail_id`, `id_project`, `judul`, `penulis`, `penerbit`, `kat_id`, `id_kertas`, `jml_hitam`, `jml_warna`, `harga_satuan`, `harga_konsumen`, `jml_cetak`, `catatan`) VALUES
(1, 1, 'Cara Coding yang Benar', 'Devara Eko', 'Bikin Buku', 'Komputer dan Internet', 2, 100, 8, 29500, 50000, 15, 'Coba lagi ya'),
(3, 3, 'Belajar PHP', 'Devara Eko', 'Bikin Buku', 'Komputer dan Internet', 1, 100, 10, 25500, 65000, 20, 'Contoh saja'),
(4, 4, 'Sinau Kawin', 'Mirza Aves Ganteng Max', 'Bikin Buku', '', 1, 100, 10, 25500, 50000, 15, 'Tolong diperbaiki penggunaan EYD nya'),
(5, 5, 'Tanam padi', 'Deva', 'Bikin Buku', 'Kesehatan', 1, 100, 10, 25500, 50000, 15, 'sadasd'),
(6, 6, 'Pemrograman Web', 'Devara Eko', 'DevStudio', 'Komputer dan Internet', 2, 100, 10, 26500, 60000, 20, 'Coba manual');

-- --------------------------------------------------------

--
-- Table structure for table `quote`
--

CREATE TABLE IF NOT EXISTS `quote` (
  `id_quote` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `quote` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `userlogin`
--

CREATE TABLE IF NOT EXISTS `userlogin` (
  `user_id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(40) NOT NULL,
  `akses` enum('admin','member','op','bos','supplier') NOT NULL,
  `photo` varchar(100) NOT NULL,
  `status` enum('aktif','banned') NOT NULL,
  `terakhir_login` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userlogin`
--

INSERT INTO `userlogin` (`user_id`, `nama`, `username`, `password`, `akses`, `photo`, `status`, `terakhir_login`) VALUES
(1, 'Administrator', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 'ava-c4ca4238a0b923820dcc509a6f75849b.jpg', 'aktif', '2016-10-19 23:43:26'),
(4, 'Operator', 'op', '4b583376b2767b923c3e1da60d10de59', 'op', '', 'aktif', '2016-11-20 16:08:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`bank_id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id_blog`);

--
-- Indexes for table `blog_cover`
--
ALTER TABLE `blog_cover`
  ADD PRIMARY KEY (`id_block_cover`);

--
-- Indexes for table `blog_kategori`
--
ALTER TABLE `blog_kategori`
  ADD PRIMARY KEY (`id_kat_blog`);

--
-- Indexes for table `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`id_buku`);

--
-- Indexes for table `buku_cover`
--
ALTER TABLE `buku_cover`
  ADD PRIMARY KEY (`id_cover`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id_faq`);

--
-- Indexes for table `file_project`
--
ALTER TABLE `file_project`
  ADD PRIMARY KEY (`id_file`);

--
-- Indexes for table `kat_buku`
--
ALTER TABLE `kat_buku`
  ADD PRIMARY KEY (`kat_id`);

--
-- Indexes for table `kertas`
--
ALTER TABLE `kertas`
  ADD PRIMARY KEY (`id_kertas`);

--
-- Indexes for table `konfigurasi`
--
ALTER TABLE `konfigurasi`
  ADD PRIMARY KEY (`konfigurasi_id`), ADD UNIQUE KEY `nama_key` (`nama_key`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paket`
--
ALTER TABLE `paket`
  ADD PRIMARY KEY (`id_paket`);

--
-- Indexes for table `paket_custom`
--
ALTER TABLE `paket_custom`
  ADD PRIMARY KEY (`id_pkt_custom`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id_project`);

--
-- Indexes for table `project_detail`
--
ALTER TABLE `project_detail`
  ADD PRIMARY KEY (`detail_id`);

--
-- Indexes for table `quote`
--
ALTER TABLE `quote`
  ADD PRIMARY KEY (`id_quote`);

--
-- Indexes for table `userlogin`
--
ALTER TABLE `userlogin`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `bank_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id_blog` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `blog_cover`
--
ALTER TABLE `blog_cover`
  MODIFY `id_block_cover` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `blog_kategori`
--
ALTER TABLE `blog_kategori`
  MODIFY `id_kat_blog` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `buku`
--
ALTER TABLE `buku`
  MODIFY `id_buku` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `buku_cover`
--
ALTER TABLE `buku_cover`
  MODIFY `id_cover` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id_faq` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `file_project`
--
ALTER TABLE `file_project`
  MODIFY `id_file` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `kat_buku`
--
ALTER TABLE `kat_buku`
  MODIFY `kat_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `kertas`
--
ALTER TABLE `kertas`
  MODIFY `id_kertas` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `konfigurasi`
--
ALTER TABLE `konfigurasi`
  MODIFY `konfigurasi_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `paket`
--
ALTER TABLE `paket`
  MODIFY `id_paket` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `paket_custom`
--
ALTER TABLE `paket_custom`
  MODIFY `id_pkt_custom` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id_project` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `project_detail`
--
ALTER TABLE `project_detail`
  MODIFY `detail_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `quote`
--
ALTER TABLE `quote`
  MODIFY `id_quote` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `userlogin`
--
ALTER TABLE `userlogin`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
